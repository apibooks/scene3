package sample.textdetection.frame;

import java.awt.Color;
import java.awt.Rectangle;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * ImageとRectangleを表示するJFrame
 * 
 * @author sakamoto
 *
 */
public class ImageFrameMain {
	public static final String imageUri = "https://images.unsplash.com/photo-1520719627573-5e2c1a6610f0?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=74f3bdd5f59b24b0bed9769b33cf8e97&auto=format&fit=crop&w=1500&q=80";

	public static void main(String[] args) throws MalformedURLException {
		ImageFrame ui = new ImageFrame("Compare");
		ui.setImage(new URL(imageUri));
		List<ImageFrameNode> nodes = new ArrayList<>();

		// the result of face detection from Microsoft Azure Cloud Service
		nodes.add(new ImageFrameNode(Color.red, new Rectangle(512, 363, 347, 347)));
		ui.addNodes(nodes);

		nodes.clear();

		// the result of face detection from Google Cloud platform
		nodes.add(new ImageFrameNode(Color.blue, new Rectangle(506, 193, 1029 - 506, 800 - 193)));
		nodes.add(new ImageFrameNode(Color.blue, new Rectangle(504, 333, 925 - 504, 754 - 333)));

		ui.addNodes(nodes);
		ui.setVisible(true);
	}

}
