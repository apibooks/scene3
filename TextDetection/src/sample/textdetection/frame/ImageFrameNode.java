package sample.textdetection.frame;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * 
 * @author sakamoto
 *
 */
public class ImageFrameNode {

	public ImageFrameNode(Color c, Rectangle r) {
		this(c, r, "");
	}

	public ImageFrameNode(Color c, Rectangle r, String text) {
		this(c, r, Collections.singletonList(text));
	}

	public ImageFrameNode(Color c, Rectangle r, List<String> textList) {
		this.color = c;
		this.rectangle = new Rectangle(r.x, r.y, r.width, r.height);
		this.textList.addAll(textList);
	}

	public Rectangle rectangle = new Rectangle();
	public Color color = Color.black;
	public List<String> textList = new ArrayList<String>();

}
