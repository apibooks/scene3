package sample.textdetection.mapper;

public class GCPFullTextElement {

	public GCPProperty property;
	public GCPBoundingBox boundingBox;
	
	public String confidence;
}
