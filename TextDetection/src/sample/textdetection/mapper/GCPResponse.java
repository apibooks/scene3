package sample.textdetection.mapper;

public class GCPResponse {

	public GCPFaceAnnotation[] faceAnnotations;
	public GCPSafeSearchAnnotation safeSearchAnnotation;
	public GCPTextAnnotation[] textAnnotations;
	public GCPFullTextAnnotation fullTextAnnotation;
	
}
