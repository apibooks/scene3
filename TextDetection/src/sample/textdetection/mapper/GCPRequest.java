package sample.textdetection.mapper;
/** JSONフォーマットの「request」 */
public class GCPRequest {
    public GCPImage image;
    public GCPFeature[] features;
}
