package sample.textdetection.mapper;

public class GCPSafeSearchAnnotation {
	public GCPLikelihood adult;
	public GCPLikelihood spoof;
	public GCPLikelihood medical;
	public GCPLikelihood violence;
	public GCPLikelihood racy;

}
