package sample.textdetection.mapper;

public class GCPTextAnnotation {

	public String locale;
	public String description;
	public GCPBoundingPoly boundingPoly;
}
