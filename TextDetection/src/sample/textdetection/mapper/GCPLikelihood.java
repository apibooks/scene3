package sample.textdetection.mapper;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum GCPLikelihood {

	UNKNOWN, VERY_UNLIKELY, UNLIKELY, POSSIBLE, LIKELY, VERY_LIKELY;

	@JsonValue
	public String toValue() {
		return name();
	}

	@JsonCreator
	public static GCPLikelihood fromValue(String value) {
		return Arrays.stream(values()).filter(v -> v.name().equalsIgnoreCase(value)).findFirst()
				.orElseThrow(() -> new IllegalArgumentException(value));
	}
}
