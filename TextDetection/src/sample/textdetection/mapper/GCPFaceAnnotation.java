package sample.textdetection.mapper;

public class GCPFaceAnnotation {
	public GCPBoundingPoly boundingPoly;
	public GCPBoundingPoly fdBoundingPoly;
	public GCPLandmarks[] landmarks;
	public int rollAngle;
	public int panAngle;
	public int tiltAngle;
	public int detectionConfidence;
	public int landmarkingConfidence;
	public GCPLikelihood joyLikelihood;
	public GCPLikelihood sorrowLikelihood;
	public GCPLikelihood angerLikelihood;
	public GCPLikelihood surpriseLikelihood;
	public GCPLikelihood underExposedLikelihood;
	public GCPLikelihood blurredLikelihood;
	public GCPLikelihood headwearLikelihood;

}
