package samples;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class OpenCVSample {

    public static void main(String[] args) {

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        /* イメージをグレースケール画像として読み込む */
        Mat src = Imgcodecs.imread("./img/test.jpg", Imgcodecs.IMREAD_GRAYSCALE);
        Mat dst = Imgcodecs.imread("./img/test.jpg");

        /* 2値化 */
        Imgproc.threshold(src, src, 0, 255, Imgproc.THRESH_BINARY_INV | Imgproc.THRESH_OTSU);

        /* 画像の輪郭を抽出 */
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(src, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        /* 矩形図を抽出 */
        List<MatOfPoint> rectContours = new ArrayList<>();
        for (MatOfPoint contour : contours) {
            /* ポリゴン近似 */
            MatOfPoint2f curve = new MatOfPoint2f(contour.toArray());
            MatOfPoint2f approxCurve = new MatOfPoint2f();
            Imgproc.approxPolyDP(curve, approxCurve, 100.0f, true);
            /* 角が4つある輪郭を集める */
            if (approxCurve.total() == 4) {
                rectContours.add(contour);
            }
        }
        /* 四角を書く */
        Imgproc.drawContours(dst, rectContours, -1, new Scalar(0, 0, 255), 5);
        /* 画像に輪郭を書き出す */
        Imgcodecs.imwrite("./img/test_grayed.jpg", dst);
    }
}
